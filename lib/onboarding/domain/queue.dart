import 'package:sec_1_morning/onboarding/data/repository/get_onboarding_items.dart';

class Queue {
  final List<Map<String, String>> sp = [];


  void push(Map<String, String> elem){
    sp.insert(sp.length, elem);
  }

  void pop(){
    sp.remove(sp[0]);
  }
  Map<String, String> peek(){
    return sp[0];
  }


  int len(){
    return sp.length;
  }

  bool is_empty(){
    return sp.isEmpty;
  }

  void reset_queue(){
    sp.clear();
    for (var elem in get_onboarding_items()){
      sp.add(elem);
    }

  }

}