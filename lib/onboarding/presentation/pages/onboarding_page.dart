import 'package:flutter/material.dart';
import 'package:sec_1_morning/onboarding/domain/queue.dart';
import 'package:sec_1_morning/onboarding/presentation/pages/holder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({super.key});

  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  var controller = PageController();
  SharedPreferences? sharedPreferences;
  int _counter = 0;
  Queue queue = Queue();
  Map<String, String> current_element = {};

  @override
  void initState(){
    super.initState();
    SharedPreferences.getInstance().then((value) async{
      sharedPreferences = value;
      _counter = await getCounter();
      queue.reset_queue();
      for (int i=0; i < _counter;i++){
        queue.pop();
      }
      if (queue.is_empty()){
        setState(() {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Holder())
          );
        });
      }
      else{
        setState(() {
          current_element = queue.peek();
        });
      }
    });
  }

  Future<int> getCounter()async{
    return sharedPreferences!.getInt('counter') ?? 0;
  }

  Future<void> setCounter(int value)async{
    sharedPreferences!.setInt('counter', value);
  }

  void _incrementCounter() async{
    _counter ++;
    setCounter(_counter);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: (sharedPreferences == null) ? const Center(child: CircularProgressIndicator()) :
      SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: PageView.builder(
          allowImplicitScrolling: false,
          controller: controller,
          itemBuilder: (_, index){
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 111,),
                SizedBox(
                  width: double.infinity,
                  child: Image.asset(current_element['image']!),
                ),
                SizedBox(height: 48,),
                Text(
                  current_element['title']!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 24,
                      color: Color(0xFF0560FA),
                      fontWeight: FontWeight.w700
                  ),
                ),
                SizedBox(height: 5,),
                Text(
                  current_element['text']!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16,
                      color: Color(0xFF3A3A3A),
                      fontWeight: FontWeight.w400
                  ),
                ),
                SizedBox(height: 82,),
                (queue.len() == 1) ?
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          height: 46,
                          width: 342,
                          child: FilledButton(
                            style: FilledButton.styleFrom(
                                backgroundColor: Color.fromARGB(255, 5, 96, 250),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)
                                )),
                            onPressed: (){
                              setState(() {
                                queue.pop();
                                _incrementCounter();
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));
                              });
                            },
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Already have an account?",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                          ),
                          InkWell(
                            child: Text(
                              "Sign in",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                            ),
                            onTap: (){
                              setState(() {
                                queue.pop();
                                _incrementCounter();
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));
                              });
                            },
                          )
                        ],
                      )
                    ],
                  ),
                )
                    :
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: 50,
                        width: 100,
                        child: FilledButton(
                            onPressed: (){
                              setState(() {
                                for (int i = 0; i < queue.len(); i++){
                                  queue.pop();
                                  _incrementCounter();
                                }
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));

                              });
                            },
                            style: FilledButton.styleFrom(
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4.69),
                                    side: BorderSide(
                                        color: Color(0xFF0560FA),
                                        width: 1
                                    )
                                )),
                            child: Text(
                              "Skip",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA), fontWeight: FontWeight.w700),
                            )),
                      ),
                      SizedBox(
                        height: 50,
                        width: 100,
                        child: FilledButton(
                            onPressed: (){
                              setState(() {
                                controller.nextPage(duration: Duration(milliseconds: 5000), curve: Curves.decelerate);
                                queue.pop();
                                current_element = queue.peek();
                                _incrementCounter();

                              });
                            },
                            style: FilledButton.styleFrom(
                                backgroundColor: Color(0xFF0560FA),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.69),
                                )),
                            child: Text(
                              "Next",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Colors.white, fontWeight: FontWeight.w700),
                            )),
                      )
                    ],
                  ),
                ),

              ],
            );
          },
        )
      )
    );
  }
}
