import 'package:flutter/material.dart';
import 'package:sec_1_morning/onboarding/presentation/pages/onboarding_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: const OnBoarding(),
    );
  }
}